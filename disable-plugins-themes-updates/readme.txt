=== Hide Admin Bar Extended ===
Extended version of Hide Admin Bar From Non-Admins (https://es.wordpress.org/plugins/hide-admin-bar-from-non-admins/)

Tags: admin bar, adminbar, dashboard, membership
Requires at least: 3.1
Tested up to: 4.8.1
Stable tag: 1.0

In wp-admin menu “Hide admin bar extended” you can set with users have visible the WordPress admin bar.

== Description ==

Choose which role users hide the Wordpress Admin Bar.

== Installation ==

1. Upload the `hide-admin-bar-extended` directory to the `/wp-content/plugins/` directory of your site or upload zip from ‘Plugins’ menu.
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. Configure in `Settings | Disable updates`
4. In this version there is no update programated.

