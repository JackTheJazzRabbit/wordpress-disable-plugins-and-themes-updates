<?php
if (!defined('ABSPATH')) return;
/*
Plugin Name: Disable Plugins & Themes Updates
Plugin URI: http://www.oriolantoncampos.com/
Description: Disable Plugins & Themes Updates - by Oriol Anton Campos
Version: 1.0
Author: Oriol Anton Campos
Author URI: http://www.oriolantoncampos.com/
*/
/*
	Copyright 2018	Oriol Anton Campos	(email : admin@oriolantoncampos.com)
	Licensed under the GPLv2 license: http://www.gnu.org/licenses/gpl-2.0.html
*/
global $dptu_db_version;
$dptu_db_version = '1.0';

add_action('wp_loaded','filter_admin_bar_for_user', 5, 0);
function filter_admin_bar_for_user() {
    if (is_user_logged_in()) {
        add_filter( 'site_transient_update_plugins', 'disable_plugin_updates_call', 10 );
	add_filter( 'site_transient_update_themes', 'disable_themes_updates_call', 10 );
    }
}

function disable_plugin_updates_call( $value) {
    if (is_user_logged_in()) {
        $plugins = get_plugins();
        foreach ($plugins as $key=>$plugin) {
            if (is_object($value)) if (get_option("dpu-plugin-".$plugin["TextDomain"])) unset( $value->response[$key] );
        }
    }
    return $value;
} 

function disable_themes_updates_call( $value) {
    //print_r($value);
    if (is_user_logged_in()) {
	$themes = get_themes();
        foreach ($themes as $key=>$theme) {
            if (get_option("dpu-theme-".$theme->get("TextDomain"))) {
                unset($value->checked[$theme->get("TextDomain")]);
                unset($value->response[$theme->get("TextDomain")]);
	    }
        }
    }
    return $value;
} 

add_action('admin_menu', 'add_admin_menu_habfna');
function add_admin_menu_habfna(){
	add_submenu_page('tools.php','Disable updates', 'Disable updates', 'manage_options', 'dpu', 'dpu_callback' );
}
function dpu_callback(){
	global $wp_roles;
	global $wpdb;
	global $habe_db_version;
        
        $themes = get_themes();
        $plugins = get_plugins();
        
        print "<p>Select the plugins to disable updates";
        print "<form action='".get_site_url()."/wp-admin/admin-post.php' method='POST'>";
        print"<input type='hidden' name='action' value='dpu_disable_plugins'>";
        print "<ul>";
        $i=0;
        foreach ($plugins as $key=>$plugin) {
            if ($plugin["TextDomain"] != "disable-plugins-themes-updates") print "<li><input type='checkbox' name='dpu-plugin-".$plugin["TextDomain"]."' value='".$plugin["TextDomain"]."' ".(get_option("dpu-plugin-".$plugin["TextDomain"]) ? " checked " : "")." />".$plugin["Name"]."</li>";
        }
        print "</ul>";
	print "<hr />";
        print "<p>Select the themes to disable updates";
	print "<ul>";
        foreach ($themes as $key=>$theme) {
            print "<li><input type='checkbox' name='dpu-theme-".$theme->get("TextDomain")."' value='".$theme->get("TextDomain")."' ".(get_option("dpu-theme-".$theme->get("TextDomain")) ? " checked " : "")." />".$theme["Name"]."</li>";
        }
        print "</ul>";
        print submit_button();
        print "</form";
	
	exit();
}


add_action( 'admin_post_dpu_disable_plugins', 'dpu_disable_plugins' );
if( !function_exists("dpu_disable_plugins") && $_POST["action"] == "dpu_disable_plugins" && !$_GET["page"] ) {
	function dpu_disable_plugins() {
                $plugins = get_plugins();
		$themes = get_themes();
                $post = $_POST;
                foreach ($plugins as $key => $plugin) {
                    if ($plugin["TextDomain"] && $plugin["TextDomain"] != "disable-plugins-themes-updates") {
                        $save = ($post["dpu-plugin-".$plugin["TextDomain"]] ? "1" : "0");
                        update_option( 'dpu-plugin-'.$plugin["TextDomain"], $save );
                    }
                }
		foreach ($themes as $key => $theme) {
                        $save = ($post["dpu-theme-".$theme->get("TextDomain")] ? "1" : "0");
                        update_option( 'dpu-theme-'.$theme->get("TextDomain"), $save );
                }
		header("Location: ".get_site_url()."/wp-admin/tools.php?page=dpu&updated=1");
	}
}

// Adds the action to the hook
if ($_GET["updated"]){
add_action("admin_notices", "dpu_form_saved");
}
function dpu_form_saved() {
    ?>
    <div class="updated">
        <p><?php _e( 'Updated!', 'dpu' ); ?></p>
    </div>
    <?php
}

/*
Actualizaciones
function myplugin_update_db_check() {
    global $jal_db_version;
    if ( get_site_option( 'jal_db_version' ) != $jal_db_version ) {
        jal_install();
    }
}
add_action( 'plugins_loaded', 'myplugin_update_db_check' );

Actualizar db
global $wpdb;
$installed_ver = get_option( "jal_db_version" );

if ( $installed_ver != $jal_db_version ) {

	$table_name = $wpdb->prefix . 'liveshoutbox';

	$sql = "CREATE TABLE $table_name (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
		name tinytext NOT NULL,
		text text NOT NULL,
		url varchar(100) DEFAULT '' NOT NULL,
		PRIMARY KEY  (id)
	);";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );

	update_option( "jal_db_version", $jal_db_version );
}
*/